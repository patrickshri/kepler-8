#Import PyOpenGL libraries

from OpenGL.GL import *
from OpenGL.GLU import *
from OpenGL.GLUT import *
import sys
from math import *
import random

global anim, width, height, i
global sun_x, sun_y, sun_r
global mercury_x, mercury_y, mercury_r, mercury_a, mercury_b, mercury_i, mercury_e
global venux_x, venus_y, venus_riiiaas, venus_a, venus_b, venus_i
global earth_x, earth_y, earth_r, earth_a, earth_b, earth_i
global mars_x, mars_y, mars_r, mars_a, mars_b, mars_i
global jupiter_x, jupiter_y, jupiter_r, jupiter_a, jupiter_b, jupiter_i
global saturn_x, saturn_y, saturn_r, saturn_a, saturn_b, saturn_i
global uranus_x, uranus_y, uranus_r, uranus_a, uranus_b, uranus_i
global neptune_x, neptune_y, neptune_r, neptune_a, neptune_b, neptune_i
global text_x, text_y

# <planet>_x : x-position
# <planet>_y : y-position
# <planet>_r : radius
# <planet>_r : eccentricity
# <planet>_a : length of semi-major axis
# <planet>_b : length of semi-minor axis

text_y = 0.5

mercury_i = 0.0			# Initial time
venus_i = 0.0 			# Initial time
earth_i = 0.0			# Initial time
mars_i = 0.0			# Initial time
jupiter_i = 0.0			# Initial time
saturn_i = 0.0			# Initial time
uranus_i = 0.0			# Initial time
neptune_i = 0.0			# Initial time

mercury_r = 0.05
mercury_a = (0.47 + 0.31) / 2.0
mercury_e = 0.206
mercury_b = sqrt(mercury_a ** 2 - (mercury_e * mercury_a) ** 2)
mercury_x = mercury_a - 0.31
mercury_y = 0.0

venus_r = 0.08
venus_a = (0.728 + 0.718) / 2.0
venus_e = 0.007
venus_b = sqrt(venus_a ** 2 - (venus_e * venus_a) ** 2)
venus_x = 0.718
venus_y = 0.0

earth_r = 0.1
earth_a = 1.02
earth_e = 0.017
earth_b = sqrt(earth_a ** 2 - (earth_e * earth_a) ** 2)
earth_x = 0.47
earth_y = 0.0

mars_r = 0.15
mars_a = 1.67
mars_e = 0.093
mars_b = sqrt(mars_a ** 2 - (mars_e * mars_a) ** 2)
mars_x = 1.67
mars_y = 0.0

jupiter_r = 0.2
jupiter_a = 5.45
jupiter_e = 0.048
jupiter_b = sqrt(jupiter_a ** 2 - (jupiter_e * jupiter_a) ** 2)
jupiter_x = 5.45
jupiter_y = 0.0

saturn_r = 0.22
saturn_a = 10.0
saturn_e = 0.056
saturn_b = sqrt(saturn_a ** 2 - (saturn_e * saturn_a) ** 2)
saturn_x = 10.0
saturn_y = 0.0

uranus_r = 0.16
uranus_a = 20.1
uranus_e = 0.047
uranus_b = sqrt(uranus_a ** 2 - (uranus_e * uranus_a) ** 2)
uranus_x = 20.1
uranus_y = 0.0

neptune_r = 0.3
neptune_a = 30.3
neptune_e = 0.009
neptune_b = sqrt(neptune_a ** 2 - (neptune_e * neptune_a) ** 2)
neptune_x = 30.3
neptune_y = 0.0

width = 1366
height = 768
axrng = 10.0 * 4.2
ayrng = 5.622 * 4.2

anim = 0

def zoom_in(f):
	glScalef(f, f, f)

def zoom_out(f):
	glScalef(f, f, f)

def init():
	glClearColor(0.0, 0.0, 0.0, 1.0)
	glColor3ub(255, 255, 0)
	
	gluOrtho2D(-axrng, axrng, -ayrng, ayrng)
	
def idle():
	if anim == 1:
		glutPostRedisplay()

#Generate random stars on each iteration
def stars():
	glPointSize(1.5)
	for i in range(1366):
		glColor3ub(255, 255, 255)
		glBegin(GL_POINTS)
		glVertex2f(random.randint(-1366, 1366), random.randint(-768, 768))
		glEnd()

def sim():
	global i
	global sun_x, sun_y, sun_r
	global mercury_x, mercury_y, mercury_r, mercury_a, mercury_b, mercury_i
	global venux_x, venus_y, venu_r, venus_a, venus_b, venus_i
	global earth_x, earth_y, earth_r, earth_a, earth_b, earth_i
	global mars_x, mars_y, mars_r, mars_a, mars_b, mars_i
	global jupiter_x, jupiter_y, jupiter_r, jupiter_a, jupiter_b, jupiter_i
	global saturn_x, saturn_y, saturn_r, saturn_a, saturn_b, saturn_i
	global uranus_x, uranus_y, uranus_r, uranus_a, uranus_b, uranus_i
	global neptune_x, neptune_y, neptune_r, neptune_a, neptune_b, neptune_i
	global text_x, text_y
	
	glClear(GL_COLOR_BUFFER_BIT) 
	
	glColor3ub(255, 255, 0)
	glutSolidSphere(0.1, 50, 50)
	
	for i in range(600):
		glPointSize(random.uniform(0.5, 3.5))
		glColor3f(sin(i), cos(i), 1.0)
		glBegin(GL_POINTS)
		glVertex2f(random.randint(-1366, 1366), random.randint(-768, 768))
		glEnd()
	
	
	mercury_i += 0.01136
	mercury_x = mercury_a * cos(mercury_i) + (0.31 / 3.0)
	mercury_y = mercury_b * sin(mercury_i) 
	
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, 0.0 - text_y)
	lines = 0
	
	value_m = "Mercury:\t" + str((mercury_x, mercury_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(136, 88, 5)
	glPushMatrix()
	glTranslate(mercury_x, mercury_y, 0)
	glutSolidSphere(mercury_r, 60, 60)
	glPopMatrix()
	
	venus_i += 1.0 / 224.7
	venus_x = venus_a * cos(venus_i) + (0.718 / 3.0)
	venus_y = venus_b * sin(venus_i) 
	
	#draw_text("Venus", (venus_x, venus_y), -0.95, -0.2)
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.05 - text_y)
	lines = 0
	
	value_m = "Venus:\t" + str((venus_x, venus_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	#
	
	glColor3ub(255, 246, 146)
	glPushMatrix()
	glTranslate(venus_x, venus_y, 0)
	glutSolidSphere(venus_r, 60, 60)
	glPopMatrix()
	
	#
	earth_i += 1.0/365.2
	earth_x = earth_a * cos(earth_i) + (0.98 / 3.0)
	earth_y = earth_b * sin(earth_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.1 - text_y)
	lines = 0
	
	value_m = "Earth:\t" + str((earth_x, earth_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.40)
	lines = 0
	
	value_m = "Earth time: " + str(earth_i) + " s"
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(97, 178, 240)
	glPushMatrix()
	glTranslate(earth_x, earth_y, 0)
	glutSolidSphere(earth_r, 60, 60)
	glPopMatrix()
	#
	
	#
	mars_i += 1.0/687.2
	mars_x = mars_a * cos(mars_i) + (1.38 / 3.0)
	mars_y = mars_b * sin(mars_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.15 - text_y)
	lines = 0
	
	value_m = "Mars:\t" + str((mars_x, mars_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(237, 102, 29)
	glPushMatrix()
	glTranslate(mars_x, mars_y, 0)
	glutSolidSphere(mars_r, 60, 60)
	glPopMatrix()
	#
	
	#
	jupiter_i += 1.0/778.3
	jupiter_x = jupiter_a * cos(jupiter_i) + (4.95 / 3.0)
	jupiter_y = jupiter_b * sin(jupiter_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.20 - text_y)
	lines = 0
	
	value_m = "Jupiter:\t" + str((jupiter_x, jupiter_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(231, 156, 24)
	glPushMatrix()
	glTranslate(jupiter_x, jupiter_y, 0)
	glutSolidSphere(jupiter_r, 60, 60)
	glPopMatrix()
	#
	
	#
	saturn_i += 1.0/(365.2 * 29.46)
	saturn_x = saturn_a * cos(saturn_i) + (9.02 / 3.0)
	saturn_y = saturn_b * sin(saturn_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.25 - text_y)
	lines = 0
	
	value_m = "Saturn:\t" + str((saturn_x, saturn_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(245, 229, 48)
	glPushMatrix()
	glTranslate(saturn_x, saturn_y, 0)
	glutSolidSphere(saturn_r, 60, 60)
	glPopMatrix()
	#
	
	#
	uranus_i += 1.0/(365.2 * 84.0)
	uranus_x = uranus_a * cos(uranus_i) + (18.3 / 3.0)
	uranus_y = uranus_b * sin(uranus_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.30 - text_y)
	lines = 0
	
	value_m = "Uranus" + " " + str((uranus_x, uranus_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(130, 35, 255)
	glPushMatrix()
	glTranslate(uranus_x, uranus_y, 0)
	glutSolidSphere(uranus_r, 60, 60)
	glPopMatrix()
	#
	
	#
	neptune_i += 1.0/(365.2 * 165.0)
	neptune_x = neptune_a * cos(neptune_i) + (30 / 3.0)
	neptune_y = neptune_b * sin(neptune_i) 
	
	glColor3ub(255, 255, 255)
	
	glPushMatrix()
	glLoadIdentity()
	glRasterPos2f(-0.95, -0.35 - text_y)
	lines = 0
	
	value_m = "Neptune" + " " + str((neptune_x, neptune_y))
	
	for c in value_m:
		glutBitmapCharacter(GLUT_BITMAP_HELVETICA_12, ord(c))
	
	glPopMatrix()
	
	glColor3ub(126, 215, 240)
	glPushMatrix()
	glTranslate(neptune_x, neptune_y, 0)
	glutSolidSphere(neptune_r, 60, 60)
	glPopMatrix()
	#
	
	glFlush()
	
def keyboard(key, x, y):
	global anim
	
	if key == chr(27):
		sys.exit()
	if key == 'a':
		anim = 1
	if key == 's':
		anim = 0
	if key == 'q':
		sys.exit()
	if key == 'i':
		zoom_in(1.2)
	if key == 'o':
		zoom_out(0.8)
	if key == 'p':
		path()

def info():
	print 'Color info (red, green, blue): '
	print 'Sun: (255, 255, 0)\nMercury: (255, 0, 255)\nVenus: (255, 103, 255)\nEarth: (255/4, 100*2, 255/3)\nMars: (255, 235, 255/2)\nJupiter: (155, 135, 255/2)\nUranus; 130, 35, 255)\nSaturn: (15, 235, 255)\nNeptune: (10, 35, 155)'

def main():
	info()
	glutInit(sys.argv)
	glutInitDisplayMode(GLUT_RGB | GLUT_SINGLE)
	glutInitWindowPosition(100, 100)
	glutInitWindowSize(width, height)
	glutCreateWindow("2D Animation")
	glutDisplayFunc(sim)
	glutKeyboardFunc(keyboard)
	glutIdleFunc(idle)
	
	init()
	glutMainLoop()
	
main()
